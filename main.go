package main

import (
	"fmt"
	"log"
	"time"
	"strconv"
	"syscall"
	"os/signal"
	"os"
	"io/ioutil"
	"crypto/x509"
	"crypto/tls"

    mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	tlsCert = "tls.crt"
	tlsKey = "tls.key"
	caCert = "ca.crt"
)

var (
	connected = 0
	clients = []mqtt.Client{}
	signals = make(chan os.Signal, 1)
	stopped = false
)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		for {
			signal := <-signals
			log.Println("received signal", signal)
			switch signal {
			case syscall.SIGINT, syscall.SIGTERM:
				stopped = true
				for i, client := range clients {
					log.Printf("[client-%d] disconnecting", i)
					client.Disconnect(5000)
				}
				os.Exit(0)
			}
		}
	}()

	count, err := strconv.ParseInt(os.Args[1], 10, 64)
	if err != nil {
		log.Fatal("unable to parse integer", os.Args[1], err)
	}

	cert, err := tls.LoadX509KeyPair(tlsCert, tlsKey)
	if err != nil {
		log.Fatal("error loading ", tlsCert, " or ", tlsKey, ": ", err)
	}

	ca, err := ioutil.ReadFile(caCert)
	if err != nil {
		log.Fatal("error loading ", caCert, ": ", err)
	}

	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(ca)

	i := 0
	for int64(len(clients)) < count && !stopped {
		i++
		opts := mqtt.NewClientOptions()
		opts.AddBroker("ssl://mqtt-test.finn.io:8883")
		opts.SetClientID(fmt.Sprintf("client-%d", i))
		opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
			log.Printf("[client %d] received message: %s from topic: %s", i, msg.Payload(), msg.Topic())
		})
		
		opts.SetTLSConfig(&tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs: pool,
		})
		opts.OnConnect = func(client mqtt.Client) {
			connected++
			log.Printf("[client %d] connected (%d total)", i, connected)
		}
		opts.OnConnectionLost = func(client mqtt.Client, err error) {
			connected--
			log.Printf("[client %d] connect lost (%d total): %v", i, connected, err)
		}
		client := mqtt.NewClient(opts)
		if token := client.Connect(); token.Wait() && token.Error() != nil {
			log.Printf("[client %d] error connecting: %v", i, token.Error())
			log.Printf("%d total connected", len(clients))
			time.Sleep(time.Second/2)
			continue
		}
		clients = append(clients, client)
		time.Sleep(time.Second)
	}
	select{}
}
